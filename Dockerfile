FROM node:6-alpine
#FROM centos:7
#
#RUN curl --silent --location https://rpm.nodesource.com/setup_6.x | bash - && yum install -y nodejs

WORKDIR /usr/src/react_ui

COPY ./package.json .

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start", "-b", "0.0.0.0:3000"]
