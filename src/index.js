import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { Crawler } from './crawl.js';


/*
 * Handles the client side logic.
 * Has 4 main states (statuses):
 * 'form' - waiting for use input
 * 'creating' - negotiating with the API the creation of crawl objects
 * 'crawling' - waiting for API response of the initiated crawl
 * 'ready' - All data is ready for the user to consume
 */
class CrawlManager extends Component {
  constructor(props) {
    super(props);
    this.state = {
      crawl_id: null,
      url: '',
      depth: '',
      status: 'form',
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.changeURL = this.changeURL.bind(this);
    this.changedepth = this.changedepth.bind(this);
  }

  changedepth(event) {
    this.setState({depth: event.target.value });
  }

  changeURL(event) {
    this.setState({url: event.target.value });
  }

  createCrawl() {
    var api_endpoint = process.env.REACT_APP_API_ENDPOINT + 'crawls/';
    var jsonHeader = new Headers();
    jsonHeader.append('Content-Type', 'application/json');
    return fetch(api_endpoint, {
      method: 'POST',
      headers: jsonHeader,
      body: JSON.stringify({
        base_url: this.state.url,
        max_depth: this.state.depth,
      }),
    }).then(response => response.json());
  }

  initiateCrawl() {
    var api_endpoint = process.env.REACT_APP_API_ENDPOINT + 'crawl/'
      + this.state.crawl_id + '/initiate_crawl';
    return fetch(api_endpoint, {method: 'POST'})
  }

  validateForm() {
    if (this.state.url == '' || this.state.depth == ''){
      alert("please enter url and depth");
      return false;
    }
    return true;
  }

  handleSubmit(event) {
    event.preventDefault();
    if (!this.validateForm()){
      return;
    }
    this.setState({status: 'creating'});
    // create crawl
    this.createCrawl().then(json => {
      this.setState({crawl_id: json.id, status: 'crawling'});
      return this.initiateCrawl();
    }).then(response => {
      if (response.status == 200){
        this.setState({status: 'ready'});
      } else {
        alert("an error occured in the server");
        this.setState({status: 'form'});
      }
    });
  }

  render() {
    switch (this.state.status) {
      case 'form':
        return (
          <CrawlerForm
            changedepth={this.changedepth}
            changeURL={this.changeURL}
            handleSubmit={this.handleSubmit}/>
        )
      case 'creating':
        return (
          <div>creating</div>
        )
      case 'crawling':
        return (
          <div>crawling</div>
        )
      case 'ready':
        return (
          <Crawler crawler_id={this.state.crawl_id} />
        )
    }
  }
}

/*
  * CrawlerForm is just a form compenent for the use of the manager.
  * It forwards all events up to the host compenent
  *
  * Expected Props:
  *   - changeURL: handler that receives event
  *   - changedepth: handler that receives event
  *   - handleSubmit: handler that receives event
*/
class CrawlerForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
      depth: '',
    }

    this.changeURL = this.changeURL.bind(this);
    this.changedepth = this.changedepth.bind(this);
  }

  changedepth(event) {
    this.setState({depth: event.target.value });
    this.props.changedepth(event);
  }

  changeURL(event) {
    this.setState({url: event.target.value });
    this.props.changeURL(event);
  }

  render() {
    return (
      <form onSubmit={this.props.handleSubmit}>
      <label>
        url:<br/>
        <input type="text" name="url" value={this.state.url} onChange={this.changeURL}/> <br/>
      </label>
      <label>
        depth:<br/>
        <input type="text" name="depth" value={this.state.depth} onChange={this.changedepth}/> <br/>
      </label>
      <input type="submit" value="Crawl"/>
      </form>
    )
  }
}



ReactDOM.render(<CrawlManager />, document.getElementById('root'));
registerServiceWorker();
