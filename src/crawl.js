import React, { Component } from 'react';


/*
 Crawler and CrawledSite are used to fetch and display data from the backend REST API.
*/

export class Crawler extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.crawler_id,
      sites: [],
    };
  }

  componentDidMount() {
    this.fetch_details()
      .then(json => {
        this.setState({
          max_depth: json.max_depth,
          base_url: json.base_url
        });
      });
    this.fetch_sites()
      .then(json => {
        this.setState({sites: json});
      });
  }

  fetch_details() {
    var api_endpoint = process.env.REACT_APP_API_ENDPOINT + 'crawl/' + this.state.id;
    return fetch(api_endpoint).then(response => response.json())
  }

  fetch_sites() {
    var api_endpoint = process.env.REACT_APP_API_ENDPOINT + 'crawl/' + this.state.id + '/crawled_sites';
    return fetch(api_endpoint).then(response => response.json())
  }

  render() {
    var sites = [];
    for (var i in this.state.sites) {
      sites.push(<CrawledSite key={i} depth={this.state.sites[i].depth} url={this.state.sites[i].url}/>);
    }
    return (
      <div className="Crawler">
        <h1>Crawl: {this.state.id}</h1>
        base_url: {this.state.base_url}<br/>
        max_depth: {this.state.max_depth}
        <h2>Crawled Sites:</h2>
        {sites}
      </div>
    );
  }
}

class CrawledSite extends Component {
  render() {
    return (
      <div>
      <h3>Site</h3>
        depth: {this.props.depth} url: {this.props.url}
      </div>
    );
  }
}

